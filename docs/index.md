# Exemples PowerShell

## Manipulation de fichiers

Recherche de tous les fichiers dans le dossier et les sous-dossiers de C:\Users\documents commençant par un chiffre, contenant le mot "texte" et ayant l'extension .txt

* `Get-ChildItem -path C:\Users\documents -recurse  | Where-Object {$PSItem.Name -match '[0-9].*texte.*\.txt'}` 

Afficher le contenu d'un fichier

* `Get-Content .\fichier.txt`
 
## Objets et XML
Récupération du contenu d'un fichier XML et conversion en objet powershell

* `$x = [xml](Get-Content .\file.xml)`

## Utilisation du PowerShell
Lister l'historique des commandes

* `Get-History`

Récupérer une commande

* `Invoke-History 1`
